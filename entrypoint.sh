#!/bin/sh
# This tells our unix docker image that this is the shell script to run when the container starts up
# this script is to be run inside of our docker

set -e # If any lines fail, return the errors to the screen. Good for debugging.

# Take in template, replace environment variables, and save to file
# default location where nginx expects to find config file
envsubst </etc/nginx/default.conf.tpl >/etc/nginx/conf.d/default.conf # This is a command that replaces the environment variables in the default.conf.template file with the actual values of the environment variables. It then saves the result to the default.conf file.

# Run NGINX server in docker
nginx -g 'daemon off;'
# By default, Nginx runs as background daemon service.
# Docker is best to run with primary application in the foreground
# That way all the logs get printed to the docker log.
# Tells nginx to run in the foreground, so that it doesn't exit when the script exits.

## Now launch nginx via docker
