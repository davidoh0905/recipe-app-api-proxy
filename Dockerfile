## Watch 26 again 

# Make sure to "not" run as a root user
# Best Practice is for Docker servicero as a least-previoleged user.
# Base Image : Image that Docker starts with and we build on top of in order to create application.
FROM nginxinc/nginx-unprivileged:1-alpine
# Alpine is the most light-weight version of the NGINX image
# Alpine is Linux operating system designed for Docker containers
# No unnecessary packages allows you to add things specific to your application
LABEL matainer="davidoh0905@gmail.com"

# Copy necessary files from local host machine to Docker container
# To the location where nginx expects to find its configuration file
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# Switch me as a root user on the Docker image
# We need to create directories and changing permissions.
# Because we are going to run nginx as a non-root user, we need to make sure that the user has access to the files.
# Question: What runs the image within the instance?
# --> I guess USER nginx? When did we create that user though???

# (X) These are running IN the image. And I guess it can freely communicate with the host?
USER root

# Create a directory for static files. -p --path
RUN mkdir -p /vol/static
# Set linux permission 755: owner of file can do rwe, group can do re. public re
# Question : How are static files going to be added in here?
RUN chmod 755 /vol/static

# Create the empty file using touch
RUN touch /etc/nginx/conf.d/default.conf
# change ownership of the file to nginx user: nginx group
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
# Within the Docker, we need to run entrypoint.sh. In order to do that we need to make it executable
# THis will allow nginx user, which will be running the entrypoint.sh script to update this default.conf file

# Just copy and change the permission of entrypoint.sh so that nginx can run the script at run time!
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Switch back to nginx user, so that when build is finished, the user is set to nginx.
# when you run the docker container from docker image, it will run it as last user you switched to
# while you were running dockerfile.
USER nginx

CMD ["/entrypoint.sh"]

# docker build -t proxy .
# -t --tag, . : current directory
# So.... our EC2 instance will run "docker build -t proxy ." and generate....?
# No! We are just running the image...
# And the image holds all this info inside.
# So if you do docker run imagename --> 