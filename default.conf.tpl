# tpl is for template.
# Populate values based on environment variables
# This is nginx configuration
server {
    listen ${LISTEN_PORT};

    # Purpose of NGINX server is to serve static files
    # and pass the rest of the request to running application
    # static url?
    location /static {
        # Directory on the proxy server
        # QUESTION???
        alias /vol/static;
        # later, map a volume to our running container and add all static files.
    }
    # rest of the request that does not match /static

    location / {
        # location block. / --> catch everything else
        # uwsgi service? what is that? This is going to be running python application.
        uwsgi_pass                      ${APP_HOST}:${APP_PORT};
        # maps all requests that match the location(/), to our uwsgi service
        # In order to pass things to uwsgi service, you need to set up some default parameters
        # for nginx. add them to `uwsgi_params`, in order to successfuly "parse the request."
        include                         /etc/nginx/uwsgi_params;
        # max body size of the request that can be sent to proxy
        client_max_body_size            10M;
    }
}